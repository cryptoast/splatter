from utils import crumble, basics
import numpy as np
from sys import argv
import cv2

def arrange_3d(img):
    imgdims = img.shape

    dim = max([max([imgdims[0],imgdims[1]])*2,256])
    space = np.zeros((dim,dim,256),dtype=np.dtype('O'))
    for i in range(imgdims[0]):
        for j in range(imgdims[1]):
            b,g,r = img[i,j]

            try:
                space[b,g,r]['count'] += 1
            except TypeError:
                space[b,g,r] = {'val':(b,g,r),'count':1}

    return space

def main():
    if len(argv) > 1:
        infile = argv[1]
    else:
        infile = input('infile: ')

    if len(argv) > 2:
        outfile = argv[2]
    else:
        outfile = input('outfile: ')

    img = cv2.imread(infile)

    space = arrange_3d(img)

    crumbled_space = crumble.crumble(space)
    crumble.save_layer_0(crumbled_space,outfile=outfile)

    basics.crop_file(outfile,prefix='')

if __name__=='__main__':
    main()
