import random, cv2
import numpy as np
from enum import Enum

def xy_offsets_by_dist(dist):
    offsets = []
    for i in range(dist+1):
        a,b = i,dist-i
        offsets.append((a,b))
        offsets.append((-a,b))
        offsets.append((-a,-b))
        offsets.append((a,-b))
    return list(set(offsets))

def add_z(xy,z=0):
    x,y = xy
    return (x,y,z)

def is_free(pt,space):
    return space[pt] == 0

def dest_step(pt,step,bound=256):
    return tuple([(p+d) % bound for p,d in zip(pt,step)])

def dest_steps(pt,path,bound=256):
    #print(pt,[step.name for step in path])
    dest = pt
    for step in path:
        dest = dest_step(dest,step,bound=bound)
    return dest

def step_point(pt,space,base_ref):
    #print(pt)
    src_height = pt[2]
    if src_height == 0:
        return pt

    bound = space.shape[0]

    dest = dest_step(pt,(0,0,-1))
    if not is_free(dest,space):
        if src_height == 1:
            dist = base_ref[pt[0],pt[1]]
        else:
            dist = 0

        while not is_free(dest,space):
            if dist == 0:
                dist = 1
            #print(dist)

            offsets_2d = xy_offsets_by_dist(dist)
            offsets = [add_z(os,z=-1) for os in offsets_2d]
            random.shuffle(offsets)

            for os in offsets:
                dest = dest_step(pt,os,bound=bound)
                if is_free(dest,space):
                    break

            if not is_free(dest,space):
                dist += 1

        if src_height == 1:
            base_ref[pt[0],pt[1]] = dist
    return dest

def decrement_source(pt,space):
    src = space[pt]
    child = {'val':src['val'],'count':1}

    if src['count'] > 1:
        src['count'] -= 1
        return src,child
    else:
        return None,child

def update_point(pt,space,base_ref):
    #print(pt)
    dest = step_point(pt,space,base_ref)

    if dest != pt:
        src,child = decrement_source(pt,space)
        if src != None:
            space[pt] = src
        else:
            space[pt] = 0

        space[dest] = child
        return True, dest, src
    else:
        return False, None, None # uh oh

def update_space(pt_queue,space,base_ref):
    new_queue = []
    for pt in pt_queue:
        ret,dest,src = update_point(pt,space,base_ref)
        if ret:
            new_queue.append(dest)
            if src != None:
                new_queue.append(pt) #shit
    return new_queue

def crumble(space):
    pt_queue = []
    for i in range(256):
        for j in range(256):
            for k in range(1,256):
                if space[i,j,k] != 0:
                    pt_queue.append((i,j,k))

    a,b,_ = space.shape
    base_ref = np.zeros((a,b),dtype=np.uint8)

    try:
        while pt_queue != []:
            pt_queue = update_space(pt_queue,space,base_ref)
            print(len(pt_queue))
    except KeyboardInterrupt:
        save_layer_0(space,outfile='abort.png')

    return space

################################################################################
# === UTILS ===
################################################################################

def no_opps(path):
    if (Dir.N in path and Dir.S in path) or (Dir.E in path and Dir.W in path):
        print('hit')
        return False
    else:
        return True
        

def gen_random_space(num_pts=1000,seed=None):
    if seed:
        random.seed(a=seed)

    space = np.zeros((256,256,256),dtype=np.dtype('O'))

    for i in range(num_pts):
        r,g,b = random.randint(1,255),random.randint(1,255),random.randint(1,255)
        space[r,g,b] = {'val':(r,g,b),'count':random.randint(1,100)}

    return space

def save_layer_0(space,outfile='out.png'):
    l0 = space[:,:,0]
    dims = space.shape

    xoff,yoff = (dims[0] - 256) // 2,(dims[1] - 256) // 2
    print(dims,xoff,yoff)
    img_out = np.zeros((dims[0],dims[1],3))
    for a in range(dims[0]):
        for b in range(dims[1]):
            i,j = a-xoff,b-yoff
            if l0[i,j] == 0:
                img_out[a,b] = 0
            else:
                img_out[a,b] = l0[i,j]['val']

    cv2.imwrite(outfile,img_out)

################################################################################
# === MAIN ===
################################################################################

def main():

    seed=input('seed: ')
    num_pts = int(input('num pts: '))
    space = gen_random_space(seed=seed,num_pts=num_pts)
    
    orig_space = space.copy()

    out_space = crumble(space)

    save_layer_0(out_space,outfile=input('outfile: '))

    return orig_space, out_space

if __name__=='__main__':
    main()

